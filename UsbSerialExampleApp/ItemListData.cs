﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Runtime;
using Java.Lang;

namespace UsbSerialExampleApp
{
    public struct BeverageItem
    {
        public int imageId { get; set; }
        public string name { get; set; }
        public int price { get; set; }
        public int count { get; set; }
    }

    public struct ItemListData
    {
        public static JavaList<BeverageItem> beverageItems { get; set; }

        /*
        public JavaList<BeverageItem> GetBeverageItems()
        {
            return beverageItems;
        }
        */


        static ItemListData()
        {
            //var temp = new List<BeverageItem>();

            //AddItem(temp);
            //AddItem(temp);
            //AddItem(temp);

            //beverageItems = temp.OrderBy(i => i.name).ToList();

            beverageItems = new JavaList<BeverageItem>();
            beverageItems.Add(new BeverageItem()
            {
                imageId = Resource.Drawable.icon_americano,
                name = "Americano",
                price = 3500,
                count = 0
            });
            beverageItems.Add(new BeverageItem()
            {
                imageId = Resource.Drawable.icon_icelatte,
                name = "ice Latte",
                price = 4200,
                count = 0
            });

            beverageItems.Add(new BeverageItem()
            {
                imageId = Resource.Drawable.icon_cafemoca,
                name = "ice cafe moca",
                price = 4800,
                count = 0
            });
        }

        static public void ClearData()
        {
            //int index = 0;
            JavaList<BeverageItem> tempItems = new JavaList<BeverageItem>();

            foreach (var item in beverageItems)
            {
                BeverageItem currentItem = item;
                BeverageItem modifiedItem = (new BeverageItem()
                {
                    imageId = currentItem.imageId,
                    name = currentItem.name,
                    price = currentItem.price,
                    count = 0
                });

                tempItems.Add(modifiedItem);
                //index++;
            }

            beverageItems.Clear();

            foreach (var item in tempItems)
            {
                beverageItems.Add(item);
                //index++;
            }

            //NotifyDataSetChanged();


            //foreach (var item in beverageItems)
            //{
            //   BeverageItem tempItem = item;
            //   tempItem.count = 0;
            //}
        }

        /*
        static void AddItem(List<BeverageItem> beverageItems)
        {
            beverageItems.Add(new BeverageItem()
            {
                imageUrl = "icon_americano.png",
                name = "Americano",
                price = "3500",
                count = "0"
            });
            beverageItems.Add(new BeverageItem()
            {
                imageUrl = "icon_icelatte.png",
                name = "ice Latte",
                price = "4200",
                count = "0"
            });

            beverageItems.Add(new BeverageItem()
            {
                imageUrl = "icon_cafemoca.png",
                name = "ice cafe moca",
                price = "4800",
                count = "0"
            });
        }
        */
    }
}
