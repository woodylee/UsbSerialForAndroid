﻿using System;
using System.Collections.Generic;
using System.IO;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.IO;
using Java.Lang;
using Object = Java.Lang.Object;

namespace UsbSerialExampleApp
{
    public static class ImageManager
    {
        static Dictionary<string, Drawable> cache = new Dictionary<string, Drawable>();

        public static Drawable Get(Context context, string url)
        {
            if (!cache.ContainsKey(url))
            {
                var drawable = Drawable.CreateFromStream(context.Assets.Open(url), null);

                cache.Add(url, drawable);
            }

            return cache[url];
        }
    }
    public class ViewHolder : Object
    {
        public ImageView Photo { get; set; }
        public TextView Name { get; set; }
        public TextView Price { get; set; }
        public TextView Count { get; set; }
        public ImageButton ButtonPlus  { get; set; }
        public ImageButton ButtonMinus { get; set; }
    }

    public class CustomListAdapter : BaseAdapter<BeverageItem>
    {
        private readonly Context c;
        private JavaList<BeverageItem> items;
        static readonly string TAG = typeof(SerialConsoleActivity).Name;

        public CustomListAdapter(Context c, JavaList<BeverageItem> items)
        {
            this.c = c;
            this.items = items;
        }


        /*
        public override Object GetItem(int position)
        {
            return items.Get(position);
        }
        */

        public override BeverageItem this[int position]
        {
            get
            {
                return items[position];
            }
        }

        public override int Count
        {
            get
            {
                return items.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;

            if (view == null)
            {
                view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.CustomItemView, parent, false);

                var photo = view.FindViewById<ImageView>(Resource.Id.imageWhat);
                var name = view.FindViewById<TextView>(Resource.Id.itemName);
                var price = view.FindViewById<TextView>(Resource.Id.itemPrice);
                var count = view.FindViewById<TextView>(Resource.Id.itemCount);

                var buttonPlus  = view.FindViewById<ImageButton>(Resource.Id.iconPlus);
                var buttonMinus = view.FindViewById<ImageButton>(Resource.Id.iconMunus);

                buttonPlus.Click += (object sender, EventArgs e) =>
                {
                    //Log.Info(TAG, "Plus button clicked! ");
                    int tempCount;

                    BeverageItem currentItem = items[position];
                    BeverageItem modifiedItem = (new BeverageItem()
                    {
                        imageId = currentItem.imageId,
                        name = currentItem.name,
                        price = currentItem.price,
                        count = currentItem.count + 1
                    });

                    items.RemoveAt(position);
                    items.Insert(position, modifiedItem);
                    NotifyDataSetChanged();

                    tempCount = modifiedItem.count;

                    Log.Info(TAG, "Plus button clicked!  " + modifiedItem.name + tempCount + ", position: " + position);
                };

                buttonMinus.Click += (object sender, EventArgs e) =>
                {
                    //Log.Info(TAG, "Plus button clicked! ");
                    int tempCount = 0;
                    
                    BeverageItem currentItem = items[position];
                    if (currentItem.count > 0)
                    {
                        BeverageItem modifiedItem = (new BeverageItem()
                        {
                            imageId = currentItem.imageId,
                            name = currentItem.name,
                            price = currentItem.price,
                            count = currentItem.count - 1
                        });

                        items.RemoveAt(position);
                        items.Insert(position, modifiedItem);
                        NotifyDataSetChanged();

                        tempCount = modifiedItem.count;
                    }

                    Log.Info(TAG, "Minus button clicked!  " + tempCount + ", position: " + position);
                };

                view.Tag = new ViewHolder() { Photo = photo, Name = name, Price = price, Count = count, ButtonPlus = buttonPlus, ButtonMinus = buttonMinus};
            }

            ViewHolder holder = (ViewHolder)view.Tag;

            holder.Photo.SetImageResource(items[position].imageId);

            holder.Name.Text = items[position].name;
            holder.Price.Text = Convert.ToString(items[position].price);
            holder.Count.Text = Convert.ToString(items[position].count);



            return view;

        }
    }
}
