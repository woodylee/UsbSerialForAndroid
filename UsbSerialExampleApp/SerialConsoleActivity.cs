﻿/* Copyright 2017 Tyler Technologies Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://github.com/anotherlab/xamarin-usb-serial-for-android
 * Portions of this library are based on usb-serial-for-android (https://github.com/mik3y/usb-serial-for-android).
 * Portions of this library are based on Xamarin USB Serial for Android (https://bitbucket.org/lusovu/xamarinusbserial).
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Database;
using Android.Hardware.Usb;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using Hoho.Android.UsbSerial.Driver;
using Hoho.Android.UsbSerial.Extensions;
using Hoho.Android.UsbSerial.Util;


namespace UsbSerialExampleApp
{
    [Activity(Label = "@string/app_name", LaunchMode = LaunchMode.SingleTop)]
    public class SerialConsoleActivity : Activity
    {
        static readonly string TAG = typeof(SerialConsoleActivity).Name;

        public const string EXTRA_TAG = "PortInfo";
        const int READ_WAIT_MILLIS = 200;
        const int WRITE_WAIT_MILLIS = 200;

        const int PRINTER_WIDTH_CHAR = 48;

        const int ITEM_NAME_WIDTH = 19;
        const int UNIT_PRICE_WIDTH = 13;
        const int ITEM_COUNT_WIDTH = 10;
        const int PRICE_WIDTH = 6;

        const int ITEM_ORDER_NAME_WIDTH = 13;
        const int ITEM_ORDER_COUNT_WIDTH = 3;
        
        const int ITEM_NAME_POS = 0;
        const int UNIT_PRICE_POS = ITEM_NAME_POS + ITEM_NAME_WIDTH;
        const int ITEM_COUNT_POS = UNIT_PRICE_POS + UNIT_PRICE_WIDTH;
        const int PRICE_POS = ITEM_COUNT_POS + ITEM_COUNT_WIDTH;
        
        const byte STX = 0x02;
        const byte ETX = 0x03;
        const byte CMD_RECEIPT_PRINTING = 0x80;
        const byte CMD_ORDER_PRINTING = 0x83;

        // error code 
        const int NO_ERROR              = 0x8000;
        const int ERROR_NO_CONNECTION   = 0x8001;
        const int ERROR_PRINT           = 0x8003;
        const int ERROR_COVER_OPEN      = 0x8006;
        const int ERROR_AUTO_CUT        = 0x8007;
        const int ERROR_NO_PAPER        = 0x8008;
        const int ERROR_NEAR_END_PAPER  = 0x8009;
        const int ERROR_CODE_INIT       = 0x8FFF;

        static int cardNoCnt = 0;
        static int orderNumberCnt = 0;

        UsbSerialPort port;

        UsbManager usbManager;
        TextView titleTextView;

        public TextView totalAmountTextView;
        Button paymentButton;
        AlertDialog paymentAlertDialog;

        SerialInputOutputManager serialIoManager;

        ListView itemList;

        // Printer
        byte[] prt_Init   = { 0x1b, 0x40 };
        byte[] prt_Normal = { 0x1b, 0x21, 0x00 };
        byte[] prt_Left   = { 0x1b, 0x61, 0x30 };
        byte[] prt_Center = { 0x1b, 0x61, 0x31 };
        byte[] prt_Right  = { 0x1b, 0x61, 0x32 };
        byte[] prt_Cut = { 0x1b, 0x64, 0x04, 0x1b, 0x69 };

        byte[] prt_FontSize1x = { 0x1d, 0x21, 0x00 };
        byte[] prt_FontSize1_5x = { 0x1d, 0x21, 0x10 };
        byte[] prt_FontSize2x = { 0x1d, 0x21, 0x11 };
        byte[] prt_FontSize3x = { 0x1d, 0x21, 0x21 };

        String cardNumberRealStr = "";
        String cardNumberDispStr = "";
        long cardScanTimeMs = 0;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            Log.Info(TAG, "OnCreate");

            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.serial_console);

            usbManager = GetSystemService(Context.UsbService) as UsbManager;
            titleTextView = FindViewById<TextView>(Resource.Id.demoTitle);
            totalAmountTextView = FindViewById<TextView>(Resource.Id.totalAmount);
            paymentButton = FindViewById<Button>(Resource.Id.paymentButton);


            itemList = FindViewById<ListView>(Resource.Id.listView);
            itemList.ItemClick += OnListItemClick;
            itemList.Adapter = new CustomListAdapter(this, ItemListData.beverageItems);
            itemList.Adapter.RegisterDataSetObserver(new ItemDataSetObserver(this));

            paymentButton.Click += delegate
            {
                Button posButton;
                Button negButton;

                ///////////////////////////////////////////////////////////////////
                // al al al
                int ChkCnt = CheckItems();

                //String str = Convert.ToString(ChkCnt);
                //DisplayAlertDialog(str);

                if (ChkCnt != 0)
                {
                    DisplayAlertDialog("수량을 선택하세요.");
                    return;
                }
                ///////////////////////////////////////////////////////////////////

                Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this);

                paymentAlertDialog = dialog.Create();
                paymentAlertDialog.SetTitle("카드를 넣어 주세요");
                paymentAlertDialog.SetMessage(" ");
                paymentAlertDialog.SetButton("결제요청", async (c, ev) =>
                {
                    await PrintAsyncTask();
                });
                paymentAlertDialog.SetButton2("cancel", (c, ev) =>
                {
                    paymentAlertDialog.Dismiss();
                });
                paymentAlertDialog.Show();

                posButton = paymentAlertDialog.GetButton((int)DialogButtonType.Positive);
                negButton = paymentAlertDialog.GetButton((int)DialogButtonType.Negative);

                posButton.Enabled = false;
                negButton.Enabled = true;

                paymentAlertDialog.KeyPress += (object sender, DialogKeyEventArgs e) =>
                {
                    Log.Info(TAG, "key: " + e.KeyCode);

                    if (e.Event.Action == KeyEventActions.Down)
                    {
                        long nowTimeMs = SystemClock.UptimeMillis();
                        Log.Info(TAG, "NowTime: " + nowTimeMs + ", scanTime: " + cardScanTimeMs);

                        if ((nowTimeMs - cardScanTimeMs) > 1000)
                        {
                            cardNoCnt = 0;
                            cardNumberRealStr = "";
                            cardNumberDispStr = "";
                        }

                        char keyChar = (char)e.Event.UnicodeChar;
                        cardNumberRealStr += keyChar;
                        cardNoCnt++;
                        if (cardNoCnt <= 16)
                        {
                            if ((cardNoCnt > 4) && (cardNoCnt <= 12))
                            {
                                cardNumberDispStr += "*";
                            }
                            else
                            {
                                cardNumberDispStr += keyChar;
                            }

                            if ((cardNoCnt == 4) || (cardNoCnt == 8) || (cardNoCnt == 12))
                            {
                                cardNumberRealStr += "-";
                                cardNumberDispStr += "-";
                            }

                            //titleTextView.Text = cardNoStr;
                            if (cardNoCnt == 16)
                            {
                                Log.Info(TAG, "Card real no " + cardNumberRealStr);
                                Log.Info(TAG, "Card disp no " + cardNumberDispStr);

                                paymentAlertDialog.SetTitle("카드를 넣어 주세요");

                                //paymentAlert.Dismiss();
                                paymentAlertDialog.SetMessage("카드번호: " + cardNumberDispStr);
                                //paymentAlertDialog.Show();

                                posButton.Enabled = true;
                            }
                        }

                        cardScanTimeMs = SystemClock.UptimeMillis();

                    }
                };
            };
        }

        void printReceipt() 
        {
        
            int offset = 0;
            int dataStartOffset = 0;
            byte lenLow = 0;
            byte lenHigh = 0;
            byte LRC = 0;
            int length = 0;
            byte[] msgData = new byte[1024];
            
            var nowTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
 
            //
            //  ST32 Protocol
            //
            offset = AddMessage(msgData, offset, STX);
            offset = AddMessage(msgData, offset, lenLow);
            offset = AddMessage(msgData, offset, lenHigh);
            offset = AddMessage(msgData, offset, CMD_RECEIPT_PRINTING);
            
            dataStartOffset = offset;
            //
            //  Printing Data 
            //
            offset = AddMessage(msgData, offset, prt_Init);
            offset = AddMessage(msgData, offset, prt_Left);
            offset = AddMessage(msgData, offset, prt_Normal);
            offset = AddMessage(msgData, offset, "[판매영수증] (주)자영테크\n");
            offset = AddMessage(msgData, offset, "서울 금천구 서부샛길 606, 2204호\n");
            offset = AddMessage(msgData, offset, "대표: 김서래\n");
            offset = AddMessage(msgData, offset, "사업자등록번호: 119-86-54575\n");
            offset = AddMessage(msgData, offset, "대표번호: 02-6091-0046\n");
            if (cardNoCnt >= 16)
            {
                offset = AddMessage(msgData, offset, "카드번호: " + cardNumberDispStr + "\n");
            }
            offset = AddMessage(msgData, offset, "거래일: " + nowTime + "\n");
            offset = AddMessage(msgData, offset, "------------------------------------------------\n");
            offset = AddMessage(msgData, offset, prt_Center);
            offset = AddMessage(msgData, offset, prt_FontSize3x);
            offset = AddMessage(msgData, offset, "주문번호 : " + orderNumberCnt + "\n");
            offset = AddMessage(msgData, offset, prt_Left);
            offset = AddMessage(msgData, offset, prt_Normal);
            offset = AddMessage(msgData, offset, "================================================\n");
            offset = AddMessage(msgData, offset, "품 명"+"              "+"단 가"+"        "+"수량"+"      "+"금 액\n");
            offset = AddMessage(msgData, offset, "------------------------------------------------\n");
            offset = AddListItems(msgData, offset);
            offset = AddMessage(msgData, offset, "------------------------------------------------\n");
            offset = AddTax(msgData, offset);
            offset = AddMessage(msgData, offset, prt_FontSize2x);
            offset = AddTotal(msgData, offset);
            offset = AddMessage(msgData, offset, prt_FontSize1x);
            offset = AddMessage(msgData, offset, prt_Cut);
            
            length = offset - dataStartOffset + 1;
            lenHigh = (byte) (length / 256);
            lenLow = (byte) (length % 256);
            msgData[1] = lenLow;
            msgData[2] = lenHigh;
            
            offset = AddMessage(msgData, offset, ETX);
            
            for (int i = 0; i<offset; i++)
            {
                LRC ^= msgData[i];
            }
            
            offset = AddMessage(msgData, offset, LRC);
            WriteData(msgData);
        }


        void printOder()
        {

            int offset = 0;
            int dataStartOffset = 0;
            byte lenLow = 0;
            byte lenHigh = 0;
            byte LRC = 0;
            int length = 0;
            byte[] msgData = new byte[1024];
            var nowTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            //
            //  ST32 Protocol
            //
            offset = AddMessage(msgData, offset, STX);
            offset = AddMessage(msgData, offset, lenLow);
            offset = AddMessage(msgData, offset, lenHigh);
            offset = AddMessage(msgData, offset, CMD_ORDER_PRINTING);

            dataStartOffset = offset;
            //
            //  Printing Data 
            //
            offset = AddMessage(msgData, offset, prt_Init);
            offset = AddMessage(msgData, offset, prt_Left);
            offset = AddMessage(msgData, offset, prt_Normal);
            offset = AddMessage(msgData, offset, "==========================================\n");
            offset = AddMessage(msgData, offset, prt_Center);
            offset = AddMessage(msgData, offset, prt_FontSize3x);
            offset = AddMessage(msgData, offset, "주문번호 : " + orderNumberCnt + "\n");
            offset = AddMessage(msgData, offset, prt_Left);
            offset = AddMessage(msgData, offset, prt_Normal);
            offset = AddMessage(msgData, offset, "==========================================\n");
            offset = AddMessage(msgData, offset, prt_FontSize1_5x);
            offset = AddMessage(msgData, offset, "품 명            수량\n");
            offset = AddMessage(msgData, offset, prt_Normal);
            offset = AddMessage(msgData, offset, "==========================================\n");
            offset = AddMessage(msgData, offset, prt_FontSize1_5x);
            offset = AddOrderItems(msgData, offset);
            offset = AddMessage(msgData, offset, prt_Normal);
            offset = AddMessage(msgData, offset, "==========================================\n");
            offset = AddMessage(msgData, offset, nowTime + "\n");

            offset = AddMessage(msgData, offset, prt_Cut);

            length = offset - dataStartOffset + 1;
            lenHigh = (byte)(length / 256);
            lenLow = (byte)(length % 256);
            msgData[1] = lenLow;
            msgData[2] = lenHigh;

            offset = AddMessage(msgData, offset, ETX);

            for (int i = 0; i < offset; i++)
            {
                LRC ^= msgData[i];
            }

            AddMessage(msgData, offset, LRC);
            WriteData(msgData);
        }

        public byte[] GetNonZeroByteArrayFromString(String str)
        {
            byte[] orgBytes = Encoding.GetEncoding("ks_c_5601-1987").GetBytes(str);
            byte[] b = new byte[orgBytes.Length];

            List<byte> bytelist = new List<byte>();

            for (int i = 0; i < orgBytes.Length; i++)
            {
                if (orgBytes[i] != 0)
                {
                    bytelist.Add(orgBytes[i]);
                }
            }
            b = bytelist.ToArray();

            return b;
        }

        int AddMessage(byte[] dstArray, int offset, byte byteData)
        {
            dstArray[offset] = byteData;
            offset += 1;

            return offset;
        }

        int AddMessage(byte[] dstArray, int offset, byte[] srcArray)
        {
            int length = 0;

            length = srcArray.Length;
            Buffer.BlockCopy(srcArray, 0, dstArray, offset, length);
            offset += length;

            return offset;
        }

        int AddMessage(byte[] dstArray, int offset, String srcString)
        {
            int length = 0;

            Log.Info(TAG, "Msg: " + srcString);

            byte[] tempByteArray = GetNonZeroByteArrayFromString(srcString);
            length = tempByteArray.Length;
            Buffer.BlockCopy(tempByteArray, 0, dstArray, offset, length);
            offset += length;

            return offset;
        }

        int AddListItems(byte[] dstArray, int offset)
        {

            foreach (var item in ItemListData.beverageItems)
            {
                String str;
                String strTemp = item.name;
                int itemTotalPrice = 0;

                if (item.count > 0)
                {
                    str = strTemp;
                    for (int i = strTemp.Length; i < ITEM_NAME_WIDTH; i++)
                    {
                        str += " ";
                    }

                    strTemp = item.price.ToString("#,###");
                    str += strTemp;
                    for (int i = strTemp.Length; i < UNIT_PRICE_WIDTH; i++)
                    {
                        str += " ";
                    }

                    strTemp = Convert.ToString(item.count);
                    str += strTemp;
                    for (int i = strTemp.Length; i < ITEM_COUNT_WIDTH; i++)
                    {
                        str += " ";
                    }

                    itemTotalPrice = item.count * item.price;
                    strTemp = itemTotalPrice.ToString("#,###");
                    if (strTemp.Length < PRICE_WIDTH)
                    {
                        for (int i = strTemp.Length; i < PRICE_WIDTH; i++)
                        {
                            str += " ";
                        }
                    }

                    str += strTemp;

                    str += "\n";

                    offset = AddMessage(dstArray, offset, str);
                }
            }
            return offset;
        }

        int AddOrderItems(byte[] dstArray, int offset)
        {

            foreach (var item in ItemListData.beverageItems)
            {
                String str;
                String strTemp = item.name;

                if (item.count > 0)
                {
                    str = strTemp;
                    for (int i = strTemp.Length; i < ITEM_ORDER_NAME_WIDTH; i++)
                    {
                        str += " ";
                    }
                                     
                    str += "     ";     //2019.6.24 al

                    strTemp = Convert.ToString(item.count);
                    //str += strTemp;   //2019.6.24 al
                    for (int i = strTemp.Length; i < ITEM_ORDER_COUNT_WIDTH; i++)
                    {
                        str += " ";
                    }
                    
                    str += strTemp;

                    str += "\n";

                    offset = AddMessage(dstArray, offset, str);
                }
            }
            return offset;
        }

        int AddTax(byte[] dstArray, int offset)
        {
            int total = 0;

            foreach (var item in ItemListData.beverageItems)
            {
                total += item.price * item.count;
            }

            //
            //  1. Tax Free Price 
            //
            String strTemp = "공급가";
            String str = strTemp;
            for (int i = (strTemp.Length*2); i < ITEM_NAME_WIDTH; i++)
            {
                str += " ";
            }

            for (int i = 0; i < (UNIT_PRICE_WIDTH + ITEM_COUNT_WIDTH); i++)
            {
                str += " ";
            }

            int taxFreePrice = total * 10 / 11;
            int taxPrice = total - taxFreePrice;
  
            strTemp = taxFreePrice.ToString("#,###");
            if (strTemp.Length < PRICE_WIDTH)
            {
                for (int i = strTemp.Length; i < PRICE_WIDTH; i++)
                {
                    str += " ";
                }
            }
            str += strTemp;
            str += "\n";
            offset = AddMessage(dstArray, offset, str);


            //
            //  2. Tax  
            //
            strTemp = "부가세";
            str = strTemp;
            for (int i = (strTemp.Length*2); i < ITEM_NAME_WIDTH; i++)
            {
                str += " ";
            }

            for (int i = 0; i < (UNIT_PRICE_WIDTH + ITEM_COUNT_WIDTH); i++)
            {
                str += " ";
            }

            strTemp = taxPrice.ToString("#,###");
            if (strTemp.Length < PRICE_WIDTH)
            {
                for (int i = strTemp.Length; i < PRICE_WIDTH; i++)
                {
                    str += " ";
                }
            }
            str += strTemp;
            str += "\n";

            offset = AddMessage(dstArray, offset, str);

            return offset;
        }

        int AddTotal(byte[] dstArray, int offset)
        {
            int total = 0;

            foreach (var item in ItemListData.beverageItems)
            {
                total += item.price * item.count;
            }

            String strTemp = "합 계";
            String str = strTemp;
            for (int i = (strTemp.Length*2); i < ITEM_NAME_WIDTH/2; i++)
            {
                str += " ";
            }

            for (int i = 0; i < ((UNIT_PRICE_WIDTH + ITEM_COUNT_WIDTH)/2 - 1); i++)
            {
                str += " ";
            }

            strTemp = total.ToString("#,###");
            str += strTemp;
            if (strTemp.Length < PRICE_WIDTH)
            {
                for (int i = strTemp.Length; i < PRICE_WIDTH; i++)
                {
                    str += " ";
                }
            }
            str += "\n";

            offset = AddMessage(dstArray, offset, str);

            return offset;
        }
        protected override void OnPause()
        {
            Log.Info(TAG, "OnPause");

            base.OnPause();

            if (serialIoManager != null && serialIoManager.IsOpen)
            {
                Log.Info(TAG, "Stopping IO manager ..");
                try
                {
                    serialIoManager.Close();
                }
                catch (Java.IO.IOException)
                {
                    // ignore
                }
            }
        }

        protected async override void OnResume()
        {
            Log.Info(TAG, "OnResume");

            base.OnResume();

            var portInfo = Intent.GetParcelableExtra(EXTRA_TAG) as UsbSerialPortInfo;

            if (portInfo != null)
            {

                int vendorId = portInfo.VendorId;
                int deviceId = portInfo.DeviceId;
                int portNumber = portInfo.PortNumber;

                Log.Info(TAG, string.Format("VendorId: {0} DeviceId: {1} PortNumber: {2}", vendorId, deviceId, portNumber));

                var drivers = await MainActivity.FindAllDriversAsync(usbManager);
                var driver = drivers.Where((d) => d.Device.VendorId == vendorId && d.Device.DeviceId == deviceId).FirstOrDefault();
                if (driver == null)
                    throw new Exception("Driver specified in extra tag not found.");

                port = driver.Ports[portNumber];
                if (port == null)
                {
                    //titleTextView.Text = "No serial device.";
                    return;
                }
                Log.Info(TAG, "port=" + port);

                //titleTextView.Text = "Serial device: " + port.GetType().Name;

                serialIoManager = new SerialInputOutputManager(port)
                {
                    BaudRate = 115200,
                    DataBits = 8,
                    StopBits = StopBits.One,
                    Parity = Parity.None,
                };
                serialIoManager.DataReceived += (sender, e) =>
                {
                    RunOnUiThread(() =>
                    {
                        UpdateReceivedData(e.Data);
                    });
                };
                serialIoManager.ErrorReceived += (sender, e) =>
                {
                    RunOnUiThread(() =>
                    {
                        var intent = new Intent(this, typeof(MainActivity));
                        StartActivity(intent);
                    });
                };

                Log.Info(TAG, "Starting IO manager ..");
                try
                {
                    serialIoManager.Open(usbManager);
                }
                catch (Java.IO.IOException e)
                {
                    //titleTextView.Text = "Error opening device: " + e.Message;
                    return;
                }
            }
        }

        public class ItemDataSetObserver : DataSetObserver
        {
            Context context;
            //static readonly string TAG = "Test";
            public ItemDataSetObserver(Context context)
            {
                this.context = context;
            }

            public override void OnChanged()
            {
                //Log.Info(TAG, "Data set changed...");
                int total = 0;


                //total = ItemListData.beverageItems  beverageItems.getTotalAmount();

                foreach (var item in ItemListData.beverageItems)
                {
                    total += item.price * item.count;
                }
                //Log.Info(TAG, "Total: " + total);

                ((Activity)context).RunOnUiThread(() =>
                {
                    TextView myview = (TextView)((Activity)context).FindViewById<TextView>(Resource.Id.totalAmount);
                    myview.Text = Convert.ToString(total) + " 원";
                });
                base.OnChanged();
            }
        }

        private void ClearItemCount()
        {
            RunOnUiThread(() =>
            {
                ItemListData.ClearData();
                ((BaseAdapter)this.itemList.Adapter).NotifyDataSetChanged();
            });
        }


        void WriteData(byte[] data)
        {
            if (port != null)
            {
                port.Write(data, WRITE_WAIT_MILLIS);
            }
        }

        void DisplayAlertDialog(string msg)
        {
            RunOnUiThread(() =>
            {
                AlertDialog tempAlertDialog;

                Android.App.AlertDialog.Builder tempDialog = new AlertDialog.Builder(this);

                tempAlertDialog = tempDialog.Create();
                tempAlertDialog.SetTitle(msg);
                tempAlertDialog.SetMessage("");
                tempAlertDialog.SetButton("ok", (c, ev) =>
                {
                    tempAlertDialog.Dismiss();
                });
                tempAlertDialog.Show();
            });
        }

        int errorCode = 0;

        void UpdateReceivedData(byte[] data)
        {
            if( (data[0] == STX) && (data[6] == ETX) )
            {
                errorCode = data[4] << 8 | data[5];

                switch (errorCode)
                {
                    case NO_ERROR:
                        break;

                    case ERROR_NO_CONNECTION:
                        DisplayAlertDialog("프린터의 연결을 점검하세요.");
                        break;
                       
                    case ERROR_PRINT:
                        DisplayAlertDialog("프린터에 이상이 발생했습니다.");
                        break;

                    case ERROR_COVER_OPEN:
                        DisplayAlertDialog("프린터의 덮개가 열렸습니다.");
                        break;

                    case ERROR_AUTO_CUT:
                        DisplayAlertDialog("프린터의 cut 기능에 문제가 생겼습니다.");
                        break;

                    case ERROR_NO_PAPER:
                        DisplayAlertDialog("프린터의 용지가 없습니다.");
                        break;

                    case ERROR_NEAR_END_PAPER:
                        DisplayAlertDialog("프린터의 용지가 얼마남지 않았습니다.");
                        break;
                }
            }

        }

        public async Task PrintAsyncTask()
        {
            await Task.Run(async () =>
            {
                long startTimeMs = SystemClock.UptimeMillis();
                long nowTimeMs = 0;

                errorCode = 0;

                orderNumberCnt++;
                printReceipt();

                while(true)
                {
                    if(errorCode != 0)
                    {
                        break;
                    }

                    if ((nowTimeMs - startTimeMs) > 5000)
                    {
                        DisplayAlertDialog("응답 시간 초과");
                        break;
                    }
                    nowTimeMs = SystemClock.UptimeMillis();
                    await Task.Delay(100);
                }

                if (errorCode == NO_ERROR)
                {
                    errorCode = 0;
                    printOder();

                    ClearItemCount();
                }
                else
                {
                    orderNumberCnt--;
                }
            });
        }

        //BeverageItem myitem;
        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var myItem = itemList.GetItemAtPosition(e.Position);
            Log.Info(TAG, "Item: " + myItem.ToString() + ", Position: " + e.Position);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        // al al al
        int CheckItems()
        {
            int count = 0;
            int loopcount = 0;

            foreach (var item in ItemListData.beverageItems)
            {
                loopcount++;

                if (item.count == 0)
                    count++;                
            }

            if (loopcount == count)
                return 1;
            else
                return 0;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
